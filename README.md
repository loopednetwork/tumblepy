# Motivation

This is a basic CLI client for interfacing with Tumblr via the [PyTumblr](https://github.com/tumblr/pytumblr) SDK. The main use case is to be able to make posts from an SSH session in environments where Tumblr is otherwise inaccessible but you want to use it like a microblogging service. This can easily be run on a VPS or other Internet-facing server.

# Requirements

## Python Libraries

The project was developed with Python 3.10.5. 3rd party packages are:

- `markdownify==0.11.2`: Render HTML content to the Terminal in a sane way.
- `pytumblr==0.2.1`: SDK for connecting to Tumblr.
- `requests==2.28.1`: Used for checking base network connectivity.

## System Libraries

System libraries needed for Python library compilation.

- None

# Authentication

Authentication is currently handled via environment variables. The following are expected, and if they're missing the code will alert you to this.

- `TUMBLR_CONSUMER_KEY`
- `TUMBLR_CONSUMER_SECRET`
- `TUMBLR_TOKEN`
- `TUMBLR_TOKEN_SECRET`

Values can be managed via the [API console](https://api.tumblr.com/console).

# Goals

## Short Term

- Provide an interface for quickly and easily making text-centric posts to Tumblr.

## Short-Mid Term

- Support video posts, assuming a public URL (e.g. YouTube, Vimeo)
- Support post edit and delete.

## Mid Term

- Support the creation of posts with images from local image files (could be helpful for screenshots.)
- Support for quote posts.
- Support for chat posts.
- Create some type of real authentication flow instead of relying on it to all happen outside of the application.

## Long Term

- Account management.
- Integrate with some kind of TUI library to look better.
- Some kind of weirdness with a [cacafire](https://linux.die.net/man/1/cacafire) library or something to try to render images for use with viewing the dashboard.
