def render_menu(menu_options: list):
	"""
	Function to render a list of menu options.

	Parameters:
		(list) with the menu option text.

	Returns:
		(none)
	"""
	counter = 0
	print("")
	for menu_option in menu_options:
		counter += 1
		print(f"{counter}: {menu_option}")

def get_selection(max: int) -> int:
	"""
	Utility function to get a menu option from the user, specifying a valid
	minimum and maximum option.

	Parameters:
		(int) for the maximum valid option.

	Returns:
		(int) with the user's selection.
	"""
	while True:
		selection = input("> ")
		try:
			selection_int = int(selection)
			if selection_int >= 1 and selection_int <= max:
				break
			else:
				print("Invalid selection.")
				continue
		except ValueError:
			print(f"Value of '{selection}'' is not valid.")

	return selection_int
