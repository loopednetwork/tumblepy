#!/usr/bin/env python3
import sys

import pytumblr
import requests

from auth import authenticate
from markdownify import markdownify
from post import Post
from post_data import chat_post, link_post, quote_post, text_post, video_post
from utility import get_selection, render_menu


def choose_blog(blog_list: list) -> str:
	"""
	Function to prompt the user to select which blog should be used if there are
	multiple or just select the only blog if there is one.

	Parameters:
		(list) of blogs found for the account during client creation.

	Returns:
		(str) with the name of the selected blog.
	"""
	if len(blog_list) > 1:
		while True:
			# Prompt the user.
			print("Which blog would you like to use? Select from:")
			print(', '.join(blog_list))
			user_choice = input("Blog Choice> ")
			if user_choice in blog_list:
				return user_choice
			else:
				print("Not a valid selection!")
	elif len(blog_list) == 1:
		print(f"Only one blog found. Defaulting to: {blog_list[0]}")
		return blog_list[0]
	else:
		print(f"No blogs found! Quitting...")
		sys.exit(1)

def blog_info(blog: str, blog_details: list):
	"""
	Function to print basic blog information for whichever blog the user has
	selected.

	Parameters:
		(str) for the name of the selected blog.
		(dict) with the blog information.

	Returns:
		(none)
	"""
	current_blog_info = ([single_blog for single_blog in
		blog_details.get('user').get('blogs') if single_blog.get('name')
		== blog][0])
	print(f"{blog}:")
	print(f"\tPosts  -> {current_blog_info.get('posts')}")
	print(f"\tQueue  -> {current_blog_info.get('queue')}")
	print(f"\tDrafts -> {current_blog_info.get('drafts')}")

def get_blog_name_list(blog_dicts: list) -> list:
	"""
	Function to take the user details and return a list of blog names.

	Parameters:
		(list) containing all blog details for the associated account.

	Returns:
		(list) containing just the blog names.
	"""
	blog_list = []
	for single_blog in blog_dicts:
		blog_list.append(single_blog.get('name'))
	return blog_list

def menu() -> int:
	"""
	Main menu loop for the program where the user picks the "main" activity type
	to undertake. Other menu functions will define specifics.

	Parameters:
		(none)

	Returns:
		(int) with the user's selection.
	"""
	menu_options = ["Create New Post", "View My Posts", "Edit Account Info", "Quit"]
	render_menu(menu_options)
	return get_selection(len(menu_options))

def post_menu() -> int:
	"""
	Function to determine the type of post the user would like to make.

	Parameters:
		(none)

	Returns:
		(int) with the user's selection.
	"""
	post_types = ["Text", "Link", "Quote", "Chat", "Image", "Video"]
	render_menu(post_types)
	return get_selection(len(post_types))

def parse_posts(current_posts: list, post_count: int):
	"""
	Function to take a dictionary representing a Tumblr post and display it to the terminal.

	Parameters:
		(list) with the current posts, comprised of dictionaries.
		(int) for the number of posts to display

	Returns:
		(none)
	"""
	counter = 0
	for single_post in current_posts:
		counter += 1
		if counter <= post_count:
			if single_post.get('title'):
				print(f"Title: {single_post.get('title')}")
			if single_post.get('date'):
				print(f"Date: {single_post.get('date')}")
			if single_post.get('short_url'):
				print(f"URL: {single_post.get('short_url')}\n")
			if single_post.get('text'):
				print(f"Quote: {markdownify(single_post.get('text'))}")
			if single_post.get('source'):
				print(f"Source: {single_post.get('source')}\n")
			if single_post.get('permalink_url'):
				print(f"Media Link: {single_post.get('permalink_url')}")
			if single_post.get('body'):
				print(f"Content:\n\n{markdownify(single_post.get('body'))}\n")
			elif single_post.get('caption'):
				print(f"Content:\n\n{markdownify(single_post.get('caption'))}\n")
			if single_post.get('description'):
				print(f"Content:\n\n{markdownify(single_post.get('description'))}\n")
			if single_post.get('url'):
				print(f"URL: {single_post.get('url')}\n")

			print("Enter for next or Q to quit.")
			user_input = input("> ")
			if user_input.lower() == "q":
				break


# Main method.
def main():
	# Validate the credentials are know.
	tumblr = authenticate()

	# Get the basic information about the user.
	try:
		user_info = tumblr.info()
	except requests.exceptions.ConnectionError:
		print("Unable to connect to Tumblr! Are you online?")
		sys.exit(1)
	greeting = f"__==Welcome, {user_info.get('user').get('name')}==__"
	print(greeting)
	print("#" * len(greeting))

	# Get a list of blog names and have the user choose one if needed.
	blog_list = get_blog_name_list(user_info.get('user').get('blogs'))
	blog_choice = choose_blog(blog_list)

	# Print some basic details about the selected blog.
	blog_details = ([single_blog for single_blog in
		user_info.get('user').get('blogs') if single_blog.get('name')
		== blog_choice])
	blog_info(blog_choice, user_info)

	# Go into the main menu loop.
	while True:
		selection = menu()

		if selection == 4:
			print("Quitting...")
			sys.exit(0)
		elif selection == 1:
			# Create a post object.
			current_post = Post()
			post_type = post_menu()
			if post_type == 1:
				# Create a text post.
				post_info = text_post()
				tumblr.create_text(blog_choice,
					state=post_info.state,
					title=post_info.title,
					body=post_info.body,
					tags=post_info.tags,
					format="markdown")
			elif post_type == 2:
				# Create a link post.
				post_info = link_post()
				tumblr.create_link(blog_choice,
					state=post_info.state,
					title=post_info.title,
					description=post_info.description,
					url=post_info.url,
					tags=post_info.tags,
					format="markdown")
			elif post_type == 3:
				# Create a quote post
				post_info = quote_post()
				tumblr.create_quote(blog_choice,
					state=post_info.state,
					quote=post_info.quote,
					source=post_info.source,
					tags=post_info.tags,
					format="markdown")
			elif post_type == 4:
				post_info = chat_post()
				tumblr.create_chat(blog_choice,
					state=post_info.state,
					title=post_info.title,
					conversation=post_info.conversation,
					tags=post_info.tags,
					format="markdown")
			elif post_type == 6:
				# Create a video post.
				post_info = video_post()
				tumblr.create_video(blog_choice,
					state=post_info.state,
					caption=post_info.caption,
					embed=post_info.url,
					tags=post_info.tags,
					format="markdown")
		elif selection == 2:
			# Print the user's posts
			posts_info = tumblr.posts(blog_choice)
			parse_posts(posts_info.get('posts'), 5)

# Catch-all when executing directly... which will basically be always.
if __name__ == "__main__":
	main()
