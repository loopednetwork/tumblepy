import sys

from post import Post
from utility import get_selection, render_menu


def get_state() -> str:
	"""
	Function to get the state of the post.

	Parameters:
		(none)

	Returns:
		(str) with the selected option.
	"""
	print("\nHow should the post be made?")
	post_scheduling = ['Published', 'Draft', 'Queue', 'Private']
	render_menu(post_scheduling)
	option_selected = get_selection(4)
	if option_selected == 1:
		return 'published'
	elif option_selected == 2:
		return 'draft'
	elif option_selected == 3:
		return 'queue'
	elif option_selected == 4:
		return 'private'

def get_short_content(description: str) -> str:
	"""
	Function to get short (single line) content from the user.

	Parameters:
		(str) with the description to display.

	Returns:
		(str) with the value of the user's content.
	"""
	print(f"\n{description}")
	return input("> ")

def get_long_content(content_description: str, extra_content: str = ""):
	"""
	Function to get the body for a post. Markdown is expected.

	Parameters:
		(str) dictating if the content is for the "body" or "description".

	Returns:
		(str) with the body in Markdown.
	"""
	# Only print extra content if it was provided.
	if extra_content != "":
		print(f"\n{extra_content}")
	print(f"\nEnter your post {content_description}. Markdown is respected other than chats! Use 'Ctrl + d' to signal the end.")
	user_content = sys.stdin.read().strip()
	if not user_content.endswith('\n'):
		# Ensure there's one empty line to avoid the UI looking worse
		# Should basically always hit this due to the strip()
		print()
	return user_content

def get_tags():
	"""
	function to get the tags for a post.

	Paramters:
		(none)

	Returns:
		(list) with the tag values.
	"""
	tag_list = []
	print("\nEnter your desired tags, separated by commas.")
	tag_string = input("> ")

	# Parse the results of the string input.
	tag_list_dirty = tag_string.split(",")
	for single_tag in tag_list_dirty:
		tag_list.append(single_tag.strip())
	return tag_list

def text_post() -> Post:
	"""
	Function to create a post object.

	Parameters:
		(none)

	Returns:
		(Post) with the object to post.
	"""
	# Get the required details from the user.
	state = get_state()
	title = get_short_content("Enter the title for your new post.")
	body = get_long_content("body")
	tags = get_tags()

	# Create the post object and return it.
	return Post(state=state, title=title, body=body, tags=tags)

def link_post() -> Post:
	"""
	Function to create a Tumblr link post.

	Parameters:
		(none)

	Returns:
		(Post) with the object to post.
	"""
	# Get the required details from the user
	state = get_state()
	description = get_long_content("description")
	url = get_short_content("Enter the URL to feature in the post.")
	tags = get_tags()

	# Create the post object and return it.
	return Post(state=state, description=description, url=url, tags=tags)

def quote_post() -> Post:
	"""
	Function to create a Tumblr quote post.

	Parameters:
		(none)

	Returns:
		(Post) with the object to post.
	"""
	# Get the required details from the user
	state = get_state()
	quote = get_long_content("quote")
	source = get_short_content("Enter the source of your quote.")
	tags = get_tags()

	# Create the post object and return it.
	return Post(state=state, quote=quote, source=source, tags=tags)

def video_post() -> Post:
	"""
	Function to create a Tumblr video post.

	Parameters:
		(none)

	Returns:
		(Post) with the object to post.
	"""
	# Get the required details from the user.
	state = get_state()
	caption = get_long_content("caption")
	url = get_short_content("Enter the video URL.")
	tags = get_tags()

	# Create the post object and return it.
	return Post(state=state, caption=caption, url=url, tags=tags)

def chat_post() -> Post:
	"""
	Function to create a Tumblr chat post.

	Parameters:
		(none)

	Returns:
		(Post) with the object to post.
	"""
	# Get the required details from the user.
	state = get_state()
	title = get_short_content("Enter the title for your new post.")
	extra_content = "Should be specified in the format of:\n\nPerson 1: Conversation content."
	extra_content += "\nPerson 2: Conversation content."
	conversation = get_long_content("chat", extra_content=extra_content)
	tags = get_tags()

	# Create the post object and return it
	return Post(state=state, title=title, conversation=conversation, tags=tags)
