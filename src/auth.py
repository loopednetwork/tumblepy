import json
import os
import sys

import pytumblr


def get_client(keys: dict) -> pytumblr.TumblrRestClient:
	"""
	Function to attempt to create a Tumblr client object based on the dictionary
	file with key information.

	Parameters:
		(dict) with the keys

	Returns:
		(pytumblr.TumblrRestClient) client object.
	"""
	try:
		return pytumblr.TumblrRestClient(
			keys.get('oauth_consumer_key'),
			keys.get('oauth_consumer_secret'),
			keys.get('oauth_token'),
			keys.get('oauth_token_secret')
			)
	except Exception as e:
		print(f"Error creating Tumblr client. Are your keys correct? Error: {e}")
		sys.exit(1)

def authenticate() -> pytumblr.TumblrRestClient:
	"""
	Function to validate the credentails from environment variables and then return a
	Tumblr client based on the pytumblr SDK.

	Parameters:
		(none)

	Returns:
		(pytumblr.TumblrRestClient) authenticated client object.
	"""
	# Validate the environment variables.
	if (os.environ.get('TUMBLR_CONSUMER_KEY')
		and os.environ.get('TUMBLR_CONSUMER_SECRET')
		and os.environ.get('TUMBLR_TOKEN')
		and os.environ.get('TUMBLR_TOKEN_SECRET')):
		# Create the client.
		return get_client(
			{'oauth_consumer_key': os.environ.get('TUMBLR_CONSUMER_KEY'),
			'oauth_consumer_secret': os.environ.get('TUMBLR_CONSUMER_SECRET'),
			'oauth_token': os.environ.get('TUMBLR_TOKEN'),
			'oauth_token_secret': os.environ.get('TUMBLR_TOKEN_SECRET')})
	else:
		error_message = "Missing environment variable! Ensure you have "
		error_message += "TUMBLR_CONSUMER_KEY, TUMBLR_CONSUMER_SECRET, "
		error_message += "TUMBLR_TOKEN, and TUMBLR_TOKEN_SECRET."
