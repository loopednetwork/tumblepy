class Post:
	def __init__(self, tags: list = [], state: str = "published",
		title: str = "", body: str = "", data: str = "",
		source: str = "", url: str = "", conversation: str = "",
		description: str = "", quote: str = "", caption: str = ""):
		self.state = state
		self.tags = tags
		self.title = title
		self.body = body
		self.data = data
		self.source = source
		self.url = url
		self.conversation = conversation
		self.description = description
		self.quote = quote
		self.caption = caption

	def stringify(self):
		if self.title:
			print(f"Title: {self.title}")
		if self.tags:
			print(f"Tags: {self.tags}")
		if self.body:
			print(f"Body: {self.body}")
